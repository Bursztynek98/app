
import javax.swing.*;
import java.awt.*;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Btn extends JButton {
    public Btn(String label) {
        super(label);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.setBorderPainted(true);
        super.setMargin(new Insets(0, 0, 0, 0));
        super.setBorder(new LineBorder(Color.BLACK));
        super.setContentAreaFilled(false);
        super.paintComponent(g);
    }

}