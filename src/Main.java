import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 400);
        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new FlowLayout());

        JTextField t1=new JTextField("TEXT");
        t1.setBounds(50,150, 200,30);
        contentPane.add(t1);

        Btn button = new Btn("X");
        contentPane.add(button);
        JButton button1 = new JButton("Y");
        contentPane.add(button1);
        JButton button2 = new JButton("Z");
        contentPane.add(button2);

        X1 x = new X1();

        System.out.print(x.x);

        X2 x2 = new X2();

        System.out.print(x2.x);

        frame.setVisible(true);
    }

}
